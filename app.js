/**
 * Vérifie si le bouton est valide
 * @param button
 * Bouton en question
 * @returns {boolean}
 * Boolean
 */
function estValide(button) {
    return button.innerHTML.length == 0;
}

/**
 * Définie le symbole sur le bouton
 * @param btn
 * Bouton
 * @param symbole
 * Symbole
 */

function setSymbol(btn, symbole) {
    btn.innerHTML = symbole;
}

/**
 * Permet de récupérer le vainqueur
 * @param pions
 * Pions
 * @param joueurs
 * Joueurs
 * @param tour
 * Tour
 * @returns {boolean}
 * Boolean
 */

function rechercherVainqueur(pions, joueurs, tour) {
    if (
        pions[0].innerHTML == joueurs[tour] &&
        pions[1].innerHTML == joueurs[tour] &&
        pions[2].innerHTML == joueurs[tour]
    ) {
        pions[0].style.backgroundColor = "#9ACD32";
        pions[1].style.backgroundColor = "#9ACD32";
        pions[2].style.backgroundColor = "#9ACD32";
        return true;
    }

    if (
        pions[3].innerHTML == joueurs[tour] &&
        pions[4].innerHTML == joueurs[tour] &&
        pions[5].innerHTML == joueurs[tour]
    ) {
        pions[3].style.backgroundColor = "#9ACD32";
        pions[4].style.backgroundColor = "#9ACD32";
        pions[5].style.backgroundColor = "#9ACD32";
        return true;
    }

    if (
        pions[6].innerHTML == joueurs[tour] &&
        pions[7].innerHTML == joueurs[tour] &&
        pions[8].innerHTML == joueurs[tour]
    ) {
        pions[6].style.backgroundColor = "#9ACD32";
        pions[7].style.backgroundColor = "#9ACD32";
        pions[8].style.backgroundColor = "#9ACD32";
        return true;
    }

    if (
        pions[0].innerHTML == joueurs[tour] &&
        pions[3].innerHTML == joueurs[tour] &&
        pions[6].innerHTML == joueurs[tour]
    ) {
        pions[0].style.backgroundColor = "#9ACD32";
        pions[3].style.backgroundColor = "#9ACD32";
        pions[6].style.backgroundColor = "#9ACD32";
        return true;
    }

    if (
        pions[1].innerHTML == joueurs[tour] &&
        pions[4].innerHTML == joueurs[tour] &&
        pions[7].innerHTML == joueurs[tour]
    ) {
        pions[1].style.backgroundColor = "#9ACD32";
        pions[4].style.backgroundColor = "#9ACD32";
        pions[7].style.backgroundColor = "#9ACD32";
        return true;
    }

    if (
        pions[2].innerHTML == joueurs[tour] &&
        pions[5].innerHTML == joueurs[tour] &&
        pions[8].innerHTML == joueurs[tour]
    ) {
        pions[2].style.backgroundColor = "#9ACD32";
        pions[5].style.backgroundColor = "#9ACD32";
        pions[8].style.backgroundColor = "#9ACD32";
        return true;
    }

    if (
        pions[0].innerHTML == joueurs[tour] &&
        pions[4].innerHTML == joueurs[tour] &&
        pions[8].innerHTML == joueurs[tour]
    ) {
        pions[0].style.backgroundColor = "#9ACD32";
        pions[4].style.backgroundColor = "#9ACD32";
        pions[8].style.backgroundColor = "#9ACD32";
        return true;
    }

    if (
        pions[2].innerHTML == joueurs[tour] &&
        pions[4].innerHTML == joueurs[tour] &&
        pions[6].innerHTML == joueurs[tour]
    ) {
        pions[2].style.backgroundColor = "#9ACD32";
        pions[4].style.backgroundColor = "#9ACD32";
        pions[6].style.backgroundColor = "#9ACD32";
        return true;
    }
}

/**
 * Vérifie si le match est à égalité (match nul)
 * @param pions
 * Pions
 * @returns {boolean}
 * Boolean
 */

function matchNul(pions) {
    for (var i = 0, len = pions.length; i < len; i++) {
        if (pions[i].innerHTML.length == 0) return false;
    }

    return true;
}

/**
 * Afficheur
 * @param element
 * Element
 * @returns {{sendMessage: setText}}
 * Texte à afficher
 * @constructor
 */
var Afficheur = function (element) {
    var affichage = element;

    function setText(message) {
        affichage.innerHTML = message;
    }

    return {sendMessage: setText};
};

/**
 * Fonction principale pour le fonctionnement du jeu.
 */

function main() {
    var pions = document.querySelectorAll("#Jeu button");
    var joueurs = ["X", "O"];
    var tour = 0;
    var jeuEstFini = false;
    var afficheur = new Afficheur(document.querySelector("#StatutJeu"));
    afficheur.sendMessage(
        "Le jeu peut commencer ! <br /> Joueur " +
        joueurs[tour] +
        " c'est votre tour."
    );
    for (var i = 0, len = pions.length; i < len; i++) {
        pions[i].addEventListener("click", function () {
            if (jeuEstFini) return;

            if (!estValide(this)) {
                afficheur.sendMessage(
                    "Case occupée ! <br />Joueur " +
                    joueurs[tour] +
                    " c'est toujours à vous !"
                );
            } else {
                setSymbol(this, joueurs[tour]);
                jeuEstFini = rechercherVainqueur(pions, joueurs, tour);

                if (jeuEstFini) {
                    afficheur.sendMessage(
                        "Le joueur " +
                        joueurs[tour] +
                        ' a gagné ! <br /> <a href="index.html">Rejouer</a>'
                    );
                    return;
                }

                if (matchNul(pions)) {
                    afficheur.sendMessage(
                        'Match Nul ! <br/> <a href="index.html">Rejouer</a>'
                    );
                    return;
                }

                tour++;
                tour = tour % 2;
                afficheur.sendMessage("Joueur " + joueurs[tour] + " c'est à vous !");
            }
        });
    }
}

main();
